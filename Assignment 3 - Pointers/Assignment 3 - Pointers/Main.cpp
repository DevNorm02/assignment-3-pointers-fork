
// Assignment 3 - Pointers
// <Your Name>


//Assignment 3 - Pointers Fork Assignment
//Devon Norman


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function
void SwapIntegers(int* first, int* second) //Pulls in the pointers to the ints 'First' and 'Second', addressing their spot in memory.
{
	//Int created used to store the inivial value of the pointer to 'first' int.
	int storeFirst = *first;

	//Sets the value of the poiner to int 'second' to be the value to the pointer of int 'first'. This is used for swapping.
	*first = *second;

	//Sets the value of the pointer to int 'second' to the value of storeFirst, which holds the original value of the pointer ot int 'first'
	*second = storeFirst;

}


// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}
